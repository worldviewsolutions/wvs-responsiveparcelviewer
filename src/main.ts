//use this one
import { enableProdMode } from '@angular/core';
import { bootstrap } from '@angular/platform-browser-dynamic';

import { ROUTER_PROVIDERS } from '@angular/router-deprecated';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { HTTP_PROVIDERS } from '@angular/http';
import {APP_ROUTER_PROVIDERS} from './app/app-component/app.routes';
import {AppComponent} from './app/app-component/app.component';
import {APP_BASE_HREF} from '@angular/common';

const ENV_PROVIDERS = [];
// depending on the env mode, enable prod mode or add debugging modules
if (process.env.ENV === 'build') {
  enableProdMode();
} else {
  ENV_PROVIDERS.push();
}

bootstrap(AppComponent, [
    // These are dependencies of our App
    ...HTTP_PROVIDERS,
    ...ROUTER_PROVIDERS,    { provide: LocationStrategy, useClass: HashLocationStrategy },
    ...ENV_PROVIDERS,
    ...APP_ROUTER_PROVIDERS
  ])
  .catch(err => console.error(err));
