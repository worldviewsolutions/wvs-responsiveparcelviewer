"use strict";
import {Component, Output, EventEmitter} from "@angular/core";

@Component({
    selector: "autocomplete-list",
    template: `<ul class="dropdown-menu  search-results">
                  <li *ngFor="let item of list">
<a  class="dropdown-item" (click)="onClick(item)"> <i class="fa fa-map-marker" aria-hidden="true"></i> {{item.text}}</a></li>
<li><a  class="dropdown-item"><i class="fa fa-search-plus" aria-hidden="true"></i> More</a></li>
               </ul>`,
    styles: [`
    .search-results { position: relative; width:100%; overflow-y:hidden; height: 90vh; top: inherit; right: -100px; display: block; padding: 0; overflow: hidden;}
    .dropdown-item {padding:20px;}
    .dropdown-item:hover {
        background-color: lightgrey;
     }
     a {
           cursor:pointer;
     }
    `]
})
export class AutocompleteList  {
    @Output() public selected = new EventEmitter();

    public list : any;

    public onClick(item: {text: string, data: any}) {
        this.selected.emit(item);
        console.log(item)
    }
}
