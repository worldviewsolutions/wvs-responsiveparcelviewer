import {Component, Input} from '@angular/core';


@Component({
    selector:'d-header',
    template:require('./header.html') ,
    styles:[`
      strong{
          font-size:15px;
      }
      
    @media(min-width :880px){
        address{
          padding-left:80px;
          padding-top:20px;
      }

    }
    
    
    
    `]
})

export class DetailHeader{
    @Input() address:string;
    @Input() owner:string;
    @Input() zip:string;
    @Input() assesment:string;
    @Input() change:string;
  
}