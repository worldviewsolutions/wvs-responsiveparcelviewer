
///<reference path="../maplabel.d.ts" />
declare var google: any;

//latest version
/*
put three black slashes then this <reference path="../typings/googlemaps/google.maps.d.ts" />
*/
import { Injectable }     from '@angular/core';
import { Http, Response, HTTP_PROVIDERS } from '@angular/http';
// Statics
import 'rxjs/add/observable/throw';
// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

//google maps api polygon vertex format
interface vertex {
    lat: number,
    lng: number
}
@Injectable()
export class parcelService {
    parcels: any;
    parcelData: parcel[];
    format(data: any): parcel[] {
        let output = new Array();
        if (data.hasOwnProperty('parcels')) {
            for (let element in data['parcels']) {
                
                output.push(new parcel(data['parcels'][element]));
            }

        } else {
            console.log('Error: invalid input' + data);
        }
        return output;
    }

    getParcel(id: string) {
        if (id.length > 0) {
            this.parcelData.forEach((p: parcel) => {
                if (p.getID() === id) return p;
            });
        } else {
            return new parcel({});
        }
    }

    constructor(http: Http) {
      let parcelURL: string;
                //fix for live and testing
                if(window.location.hostname === 'localhost') {
                parcelURL = "http://localhost:8080/parcels.json";
                } else {
                parcelURL = "http://vm102.worldviewsolutions.net/mobilePV/parcels.json";
                }
        this.parcels = http.get(parcelURL)
            .map(response => this.parcels = (this.format(response.json())));
    }
}
/**
 *  parcel - stores parcel data
 *  @param{JSON} parcelData -- concerts json string of following form into parcel
 *     {
            "parcelId": "ABC1234",
            "fields": {
                    /          "address":"2158 Brookfield Rd",
object fields  <-|           "Current value": 100.000,
                    \          "Other": "words about things"
            },
            "geometry":[
                
            ]
        }
 *  
 */
export class parcel {
    private marker: any;
    private label: any;
    private polygonGeometry: vertex[];
    private coords: number[];
    private address: string;
    private parcelID: string;
    private fields: Object;
    private thumbnail: string;

    constructor(private parcelData: Object) {
        //load required values
        this.parcelID = parcelData['parcelId'] || "no address";
        //get optional values
        this.polygonGeometry = parcelData['geometry'];

        //load fields
        this.fields = parcelData['fields'] || {};

        //see if some specific fields exist in fields
        if (this.fields.hasOwnProperty('address')) {
            this.address = this.fields['address'];
            delete this.fields['address'];
        }
        if (this.fields.hasOwnProperty('blueprint')) {
            if (this.fields['blueprint'].hasOwnProperty('images')) {
                if (this.fields['blueprint']['images'].length > 0) {
                    this.thumbnail = this.fields['blueprint']['images'][0];
                } else {
                    //get google sv image
                    let parcelCenter = this.getcoords();
                    this.thumbnail = `//maps.googleapis.com/maps/api/streetview?size=400x400&location=` + parcelCenter.lat() + `,` + parcelCenter.lng() + `
&fov=90&heading=235&pitch=10
&key=AIzaSyA-70ObVWMGDjN-1c8rd5F1RmM39oQotZ4`;

                }
            }
        }
    }
    /*
    Finds the center of parcel polygon and returns as a tuple 
    */
    getcoords() {
        let bounds = new google.maps.LatLngBounds();
        let i: number;
        for (i = 0; i < this.polygonGeometry.length; i++) {
            bounds.extend(new google.maps.LatLng(this.polygonGeometry[i].lat, this.polygonGeometry[i].lng));
        }
        return bounds.getCenter();
    }
    getSearchValues() {
        return {
            "address": this.address,
            "parcelID": this.parcelData
        }
    }
    getID(): string {
        return this.parcelID;
    }
    getAddress(): string {
        return this.address;
    }
    getValues(): Object {
        var returnPairs = {};
        for (var key in this.fields) {
            if (this.fields.hasOwnProperty(key)) {
                returnPairs[key] = this.fields[key];
            }
        }
        return returnPairs;
    }
    getInfowindow() {
        return new google.maps.InfoWindow({

            content: "<div style='overflow-x:hidden'><h4 align='center' style='font-weight:500;'>" + this.address + "</h4> <div class='row'><div  class='col-sm-6'><h5 style='font-weight:500; padding-top:7px;'>Assesment:</h5><h5 style='font-weight:600;'> " + this.fields['assesment'] + "</h5></div><div class='col-sm-6'><h5 style=' color:green; font-weight:600';><i class='fa fa-arrow-up' aria-hidden='true'></i>" + this.fields['change'] + "</h5><h5 style='font-weight:500;'>" + this.fields['LotSize'] + "<br>" + this.fields['bedrooms'] + "Br | " + this.fields['bathrooms'] + "Ba" + "</h5></div></div><div align='center'><button type='button' class='btn btn-info hidden-xs hidden-sm' data-toggle='modal' data-target='#" + this.parcelID + "'>View Full Parcel Details</button><a href='./parcel/" + this.parcelID + "'class=' hidden-lg hidden-xl hidden-md btn btn-info'>View Parcel Details</a></div></div>"

        });
    }
    getImage() {
        return this.fields['blueprint']['images'][0];
    }
    getMarker() {
        let marker = new google.maps.Marker({
            position: this.getcoords(),
        });
        marker.setIcon('http://maps.google.com/mapfiles/ms/icons/green-dot.png')
        return marker;
    }
    getLabel() {
        return MapLabel({
            text: this.fields['assessment'],

            position: this.getcoords(),
            fontSize: 35,
            align: 'right'
        });
    }
    /*    getPolygon() {
         new google.maps.Polygon({
                paths: this.polygonGeometry,
                strokeColor: '#000000',
                strokeOpacity: 0.8,
                strokeWeight: 2.5,
                fillOpacity: 0,
            });
        }
        getHighlightedPolygon() {
            return new google.maps.Polygon({
                paths: this.polygonGeometry,
                strokeColor: '#5bc0de',
                strokeOpacity: 1.0,
                strokeWeight: 9,
                fillOpacity: 0,
            });
        } */
    getPolygon() {
        return new google.maps.Polygon({
            paths: this.polygonGeometry,
            strokeColor: '#000000',
            strokeOpacity: 0.8,
            strokeWeight: 2.5,
            fillOpacity: 0.0
        });
    }
    getHighlightedPolygon() {
        return new google.maps.Polygon({
            paths: this.polygonGeometry,
            strokeColor: '#5bc0de',
            strokeOpacity: 0.8,
            strokeWeight: 9,
            fillOpacity: 0
        });
    }
}