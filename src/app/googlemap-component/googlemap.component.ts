
import {Component, AfterContentInit, OnChanges, Input, ElementRef, SimpleChange, EventEmitter, Output } from '@angular/core';
import {parcelService, parcel} from '../parcel-component/parcel.class';
import {HTTP_PROVIDERS} from '@angular/http';
declare var $: any; // jquery hack
declare var google: any;
// Fix a stupid quirk in TypeScript (http://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript)
interface window { gapi: any; }

// define global mapping structs
interface ICoords {
    lat: number;
    long: number;
};

interface IParcelGeodata {
    infobox: any//.maps.InfoWindow;
    marker: any//.maps.Marker;
    layer: any//.maps.Polygon;
};

@Component({
    selector: 'map',
    styles: [`
      .google-map-container { height: 95vh; width:100%; }
  `],

    properties: ['key', 'options', 'latitude', 'longitude', 'zoom', 'selected'],
    // outputs: ['isGoogleLoaded'],
    providers: [
        parcelService,
        HTTP_PROVIDERS
    ],
    template: '<div class="google-map-container"></div>'
})

export class GoogleMapComponent implements AfterContentInit, OnChanges {
    @Input('latitude') latitude: number;
    @Input('longitude') longitude: number;
    @Input('selected') selectedParcel: string;
    @Output() isGoogleLoaded = new EventEmitter();
    private selectedParcelgeo: any;
    private parcelData: parcel[] = [];
    /*
   Declare public vars
   */
    map: any//.maps.Map; // map object
    infobox: any//.maps.InfoWindow;
    features: Array<IParcelGeodata> = new Array();
    // get parcel Data
    private geometrySelectEvent: string = "click";
    constructor(private _parcels: parcelService, private _elementRef: ElementRef) {
        //detect if touch is supported
        if (!!('ontouchstart' in window)) {
            this.geometrySelectEvent = "mousedown";
            console.log('touch screen detected')
        }
        //get parcel data
        if (this.parcelData != null) {
            _parcels.parcels.subscribe(
                (data: parcel[]) => {
                    this.parcelData = data
                },
                (error: any) => {
                    console.error('Error: ' + error)
                });
        }
    }

    // since angular directives cannot access attribute params at construction, we have to do everything after init
    ngAfterContentInit() {

        let el = this._elementRef.nativeElement.querySelector('.google-map-container');
        let options: Object; // map options
        // make sure we got an api key
        if (this.hasOwnProperty('key')) {
            if (this.hasOwnProperty('options')) {
                options = this['options'];
                this.initMap(el, options);
            } else {
                // if no location supplied, use geolocation to center map
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition((position) => {
                        console.log(position);
                        options = {
                            center: { lat: position.coords.latitude, lng: position.coords.longitude },
                            scrollwheel: false,
                            zoom: 17
                        };
                        this.initMap(el, options);
                    }, () => {
                        console.log('Geolocation request denied');
                        options = {
                            center: { lat: this.latitude, lng: this.longitude },
                            scrollwheel: false,
                            zoom: 10
                        };
                        this.initMap(el, options);
                    });
                } else {
                    console.log('Browser does not support Geolocation');
                }
            }

            console.log('map loaded and ready to go')

        } else {
            console.log('Error: no key found');
        }
    }

    // listen for attribute changes, recenter map if a new parcel is selected
    ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
        this.centerMapOn(this.selectedParcel);
    }

    initMap(googlemaps: any, options: any) {
        // init map
        let styles = [
            {
                stylers: [
                    { saturation: -80 }
                ]
            }, {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [
                    { lightness: 50 },
                    { hue: '#00d4ff' },
                    { visibility: 'simplified' }
                ]
            }, {
                featureType: 'road',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            },
            {
                featureType: 'poi.business',
                elementType: 'labels',
                stylers: [
                    { visibility: 'off' }
                ]
            }
        ];
        let styledMap = new google.maps.StyledMapType(styles,
            { name: 'Map' });
        options['mapTypeControlOptions'] = {
            mapTypeIds: [google.maps.MapTypeId.SATELLITE, 'map_style'],
            position: google.maps.ControlPosition.LEFT_BOTTOM
        };
        options['mapTypeControl'] = true;
        options['streetViewControlOptions'] = {
            position: google.maps.ControlPosition.LEFT_TOP
        };
        options['zoomControlOptions'] = {
            position: google.maps.ControlPosition.LEFT_TOP

        };
        //        this.map.setOptions({ styles: styles });


        options['mapTypeId'] = google.maps.MapTypeId.ROADMAP; // google.maps.MapTypeId.satellite;

        this.map = new google.maps.Map(googlemaps, options);
        google.maps.event.addListenerOnce(this.map, 'idle', () => {
            // hide loading modal on parent when map is loaded
            this.isGoogleLoaded.emit({
                value: true
            })
        });

        this.map.mapTypes.set('map_style', styledMap);
        this.map.setMapTypeId('map_style');

        this.loadParcels();
        this.centerMapOn(this.selectedParcel);
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            this.loadParcels();
            console.log('zoom_changed');
        });
    }

    clearMap() {
        /*    this.map.data.forEach(function (feature: any) {
                this.map.data.remove(feature);
            }); */
    }

    /*
     loadParcels() -- loads upon complation of parcelService request
     */
    loadParcels() {
        // clear it all
        for (let key in this.features) {
            if (this.features.hasOwnProperty(key)) {

                this.features[key].marker.setMap(null);
                this.features[key].layer.setMap(null);
                this.features[key].infobox.close();
            }

        }

        // add parcel geometry to map
        this.map.overlayMapTypes.setAt(0, null);
        if (this.parcelData.length > 0) {
            if (this.features.length < 1) {
                this.parcelData.forEach(element => {
                    this.features[element.getID()] = {
                        marker: element.getMarker(),
                        infobox: element.getInfowindow(),
                        layer: element.getPolygon(),
                        highlightedLayer: element.getHighlightedPolygon()
                    }
                    if (this.map.getZoom() < 15 && this.features.hasOwnProperty(element.getID())) {
                        this.features[element.getID()].marker.setMap(this.map);
                        this.features[element.getID()].marker.addListener(this.geometrySelectEvent, (ev: any) => {
                            for (let feature in this.features) {
                                this.features[feature].infobox.close();
                            }
                            $('.selected-parcel').removeClass('selected-parcel');
                            $("#" + element.getID() + "_parcel").addClass("selected-parcel");
                            $("#" + element.getID() + "_parcel").trigger('mouseover');
                            if (this.hasOwnProperty('infobox')) this.infobox.close();
                            if (this.hasOwnProperty('selectedParcelgeo')) this.selectedParcelgeo.setMap(null);
                            this.selectedParcelgeo = this.features[element.getID()].highlightedLayer;
                            this.selectedParcelgeo.setMap(this.map);
                            this.features[element.getID()].infobox.setPosition(ev.latLng);
                            this.features[element.getID()].infobox.open(this.map, this.features[element.getID()].marker);
                        });
                    } else {
                        this.features[element.getID()].layer.setMap(this.map);
                        this.features[element.getID()].layer.addListener(this.geometrySelectEvent, (ev: any) => {
                            for (let feature in this.features) {
                                this.features[feature].infobox.close();
                            }
                            $('.selected-parcel').removeClass('selected-parcel');
                            $("#" + element.getID() + "_parcel").trigger('mouseover');
                            $("#" + element.getID() + "_parcel").addClass('selected-parcel');
                            if (this.hasOwnProperty('infobox')) this.infobox.close();
                            if (this.hasOwnProperty('selectedParcelgeo')) this.selectedParcelgeo.setMap(null);
                            this.selectedParcelgeo = this.features[element.getID()].highlightedLayer;
                            this.selectedParcelgeo.setMap(this.map);
                            this.features[element.getID()].infobox.setPosition(ev.latLng);
                            this.features[element.getID()].infobox.open(this.map, this.features[element.getID()].marker);
                        });
                    }
                });
            }
        }
    }

    /*
    addLayer() -- adds KML or GeoJSON layer to  map
    */
    addLayer(type: string, url: string) {
        switch (type) {
            case 'GeoJSON':
                this.map.data.loadGeoJson(url);
                break;
            case 'KML':
                // let ctaLayer = new google.maps.KmlLayer({
                //     'url': url,
                //     'map': this.map
                // });
                break;
            default:
                console.log('Invalid type: ' + type + ' could not load ' + url);
                break;
        }
    }

    centerMapOn(parcel: string) {
        if (this.infobox) {
            this.infobox.close();
            this.selectedParcelgeo.setMap(null);
        }
        for (let feature in this.features) {
            this.features[feature].infobox.close();
        }
        if (parcel.length > 1) {
            this.parcelData.forEach(element => {
                console.log(element.getID());
                if (element.getID() === parcel) {
                    this.selectedParcelgeo = element.getHighlightedPolygon();
                    this.selectedParcelgeo.setMap(this.map);
                    //  this.map.setCenter(element.getcoords());
                    this.infobox = element.getInfowindow();
                    this.infobox.setPosition(element.getcoords());
                    this.infobox.open(this.map);
                    this.features[element.getID()].layer.setMap(null);
                    this.features[element.getID()].layer.setMap(this.map);
                }
            });
        }
    }
};
