
import{ROUTER_DIRECTIVES,} from '@angular/router';
import {LocalStorageService} from "angular2-localstorage/LocalStorageEmitter";
import { Component } from '@angular/core';
import { dataView } from '../data-view-component/data.view.component';
import { mapView } from '../mapview-component/map.view.component';
import { disclaimerView } from '../disclaimerView-component/disclaimerView.component';
import {  searchBar } from '../search-component/search.component';

@Component({
  selector: 'app',
  styles: [`   
  .navbar  {
    margin-bottom: 20px;

    }
    #search {
      width: 50vw;
    }
    #search < input {
      width: 50vw !important;
    }
    
   `],
  template: require('./app.component.html')
  ,
  directives: [ROUTER_DIRECTIVES, searchBar ],
  providers: [
    mapView,
    dataView
  ]
})
/*
@Routes([
  {
    path: ':id',
    name: 'Main',
    useAsDefault: true,
    component: disclaimerView
  },
  {
    path: 'map/:id',
    name: 'Map',
    component: mapView
  },
  {
    path: 'parcel/:id',
    name: 'Parcel',
    component: dataView,
  },
  // fixed vm102 errors
    {
    path: 'mobilePV/map/:id',
    name: 'Map',
    component: mapView
  },
    {
    path: 'mobilePV/parcel/:id',
    name: 'Parcel',
    component: dataView,
  }
])*/
export class AppComponent {
  navbarVisible = true;
  
    

}
