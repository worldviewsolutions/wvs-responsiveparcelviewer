import {provideRouter, RouterConfig} from '@angular/router';
import { mapView } from '../mapview-component/map.view.component';
import { dataView } from '../data-view-component/data.view.component';
import { disclaimerView } from '../disclaimerView-component/disclaimerView.component';

import { parcel, parcelService } from '../parcel-component/parcel.class';
export const routes: RouterConfig = [
{ 
  path:'', 
  component:disclaimerView
},
{
  path:'map/:id', 
  component:mapView
},
{
  path:'parcel/:id', 
  component:dataView
},
{
  path:'mobilepv/parcel/', 
  component:dataView
},
{
  path:'mobilepv/map/', 
  component:mapView
}
];


export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];

