import {Component} from '@angular/core';
@Component({
  selector: 'toggleBtn',
   styles: [`    footer {
        background: white;
      }
    `],
  
  template: `<button type="button" class="btn btn-primary btn-block"><i class="fa fa-sliders" aria-hidden="true"></i> Toggle Layers</button>
`})
export class toggleBtn {

}


