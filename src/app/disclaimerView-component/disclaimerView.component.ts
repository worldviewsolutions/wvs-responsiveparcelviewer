declare var $: any; //for jquery
import {Component, AfterContentInit, Injectable, OnInit, Input} from '@angular/core';
import { searchBar } from '../search-component/search.component';
import { GoogleMapComponent } from '../googlemap-component/googlemap.component';
import { footerComponent } from '../footer-component/footer.component';

@Component({
    selector: 'disclaimer',
    template: `
  <div class="searchmodal">
         <div align="center"><img class="img-responsive" src="{{clientLogo}}"/></div>
  <div class="searchmodalcontent">
     <div class="jumbotron">
<div id="searchZone">
<h4 style="font-weight:500px;" div align="left"> Search for Parcels:</h4>
  <search-bar (keypress)=resizedropdown() data-toggle="popover" data-placement="bottom"></search-bar>
</div>
  </div>
  </div>
  </div>
<div id="mapzone">
<map selected="" (isGoogleLoaded)="onGoogleMapsLoad($event)"  height="100%" key="{{key}}"></map>
</div>
    <!-- Modal -->
<div id="disclaimer" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
<h3>Parcel Viewer</h3>
      </div>
      <div class="modal-body">
      <div class="container-fluid">
<div class="row">
   <div class="jumbotron">
    <p id="legal">The maps, geographic information, property information, and zoning information (collectively referred to as “Data”) you are about to access from Powhatan County’s geographic information system (GIS) is made available as a service to the public for providing general information. All information obtained from the GIS is provided “as is” and POWHATAN COUNTY MAKES NO EXPRESS OR IMPLIED WARRANTIES RELATING TO THE DATA INC LUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. USER ACCEPTS ALL RISKS THAT DATA MAY NOT ACCOMPLISH ANY OR ALL EXPECTATIONS OF THE USER. Powhatan County has made reasonable efforts to assure that the Data is correct, however, no guarantee is made as to the accuracy of the Data. Any determination of topography or contours, any depiction of physical improvements, property lines or boundaries, and any information concerning zoning or property assessments, are solely for general information and shall not be used or relied upon, in the design, modification, or construction of improvements to real property or for resource protection area or flood plain determination. The Data is not a legal representation of information depicted and as such, has no legal bearing on the true shape, size, area, location, zoning or existence of the real property depicted. The Data should not substitute for a professional survey or deed research. Likewise, the Data should not substitute for verifying the zoning or tax assessment of any property. Users of this site are encouraged to consult the legal documents used in the creation of the Data which can be found within the deeds and plats recorded and stored by the Clerk of the Circuit Court of Powhatan County. Users are also encouraged to verify the accuracy of the Data with the Powhatan County Commissioner of the Revenue, as well as the Powhatan County Planning Department. Powhatan County does not assume any responsibility for any errors, deficiencies, defects, omissions, or inaccuracies in the Data. Powhatan County shall not be liable to the user, or to any other person, for any actions; claims; incidental, consequential or special damages – including loss of facilities, lost profits, business interruption, loss of business information or other pecuniary loss; or judgments arising out of, or in connection with, the use or misuse, reliance on, or performance of, Data. The user of this information assumes all liability for his/her use of any Data obtained. This website displays aerial photography images obtained in different years which have been captured utilizing different methods. At the same time, vector map layers have been created and maintained separate from the aerial photography. As such, there may be areas where the map layers do not align precisely with the aerial imagery. Data displayed on this website is for informational purposes only.

</p> 
  </div>
  
  <div class="row">
  <div class="span12 centred">
   <div class="checkbox">
          <label>
            <input (click)="acceptTOS()" type="checkbox">   By checking here, you agree to the information provided above.
          </label>
        </div>
        </div>
</div>
</div>
      </div>
      </div>
      <div class="modal-footer">
        <img align="left" src="http://gis.venangopa.us/ParcelViewer/Images/WVSLogo.gif"/> <button id="enterSite" type="button" class="btn btn-success disabled" data-dismiss="modal">Enter Site</button>

      </div>
    </div>

  </div>
</div>
<footerComponent></footerComponent>
`,
    providers: [],
    directives: [footerComponent, searchBar, GoogleMapComponent],
    styles: [`
* {box-sizing:border-box;}

#legal {
    height: 35vh;
    overflow-y: scroll;
}
#main-img {
    width: 100%;
}
#mapzone {
    z-index: -1;
     position: fixed;
    left: 0px;
    top: 0px;
    height: 100%;
    width: 100%;
}
#mapzone:after {
      content: "";
  display: block;
  position: fixed; 
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 10;
  background-color: rgba(0,0,0,0.3);
}
.searchmodal {
    padding-left: 100px;
    padding-right: 100px;
}
@media (min-width: 1200px) {
.searchmodal {
    width: 1400px;
    height: 400px;
    line-height: 200px;
    position: fixed;
    top: 30%;
    left: 50%;
    margin-top: -200px;
    margin-left: -700px;
    border-radius: 5px;
    text-align: center;
    z-index: 11; /* 1px higher than the overlay layer */
}
}
@media (max-width: 1200px) {
.searchmodal {
    width: 900px;
    height: 400px;
    line-height: 200px;
    position: fixed;
    top: 30%;
    left: 50%;
    margin-top: -200px;
    margin-left: -450px;
    border-radius: 5px;
    text-align: center;
    z-index: 11; /* 1px higher than the overlay layer */
}
}

@media (max-width: 800px) {
.searchmodal {
    width: 700px;
    height: 400px;
    line-height: 200px;
    position: fixed;
    top: 30%;
    left: 50%;
    margin-top: -200px;
    margin-left: -350px;
    border-radius: 5px;
    text-align: center;
    z-index: 11; /* 1px higher than the overlay layer */
}
}
@media (max-width: 500px) {
.searchmodal {
    width: 500px;
    height: 400px;
    line-height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    margin-top: -200px;
    margin-left: -250px;
    border-radius: 5px;
    text-align: center;
    z-index: 11; /* 1px higher than the overlay layer */
}
}

#search {
    width:100% !important;
}
.searchmodalcontent {
    margin: 30px;
    opacity:.9;
   
    
}
.checkbox {
    text-align: center;
}
.searchHeader{
     color:#BC4423;
    postion:absoulute;
    border: solid;
    border-width: 1px 1px 0 1px;
   
}
    `],
})
export class disclaimerView implements OnInit, AfterContentInit {
    clientLogo = 'http://www.danville-va.gov/ImageRepository/Document?documentID=15290';
    key = "AIzaSyA-70ObVWMGDjN-1c8rd5F1RmM39oQotZ4";
    //@LocalStorage() 
    public tosAccepted: boolean = false;

    private logo: string = '/img/WVS_Logo_Black_large.png';
    constructor() {
    }
    resizedropdown(e: any) {
        $('.search-results').width($('#search').width() / 2);
    }
    ngAfterContentInit() {
        $('#search').hide();
    }
    onGoogleMapsLoad(e: any) {
        console.log(e);
    }
    acceptTOS(event) {
        this.tosAccepted = true;
        $("#enterSite").toggleClass("disabled");
    }
    ngOnInit() {
        $('#disclaimer').modal({
            keyboard: false
        })
        $('#disclaimer').on('hidden.bs.modal', (e: any) => {
            console.log(this.tosAccepted);
            if (this.tosAccepted === false) {
                $('#disclaimer').modal({
                    keyboard: false
                });
            }
        })
    }
}
