declare var d3: any;
declare var $: any;
import {Component, Input, AfterContentInit, OnInit, ElementRef}  from '@angular/core';

@Component({
    selector: 'd3graph',
    template: ` `,
    styles: [`




    `],
    properties: ['color', 'data','parcelId']
})

export class d3graph implements AfterContentInit, OnInit {
    @Input() data = new Array();
    @Input() color: string;
    id: string;
    yTitle: string = "value ($)";
    elementRef: ElementRef;
    constructor(elementRef: ElementRef) {
        this.elementRef = elementRef;
    }
    ngOnInit() {
        if (this.hasOwnProperty('parcelId')) {
            this.id = this['parcelId']
        } else {
            this.id = ''
        }
        var dataset = [{"value":45000,"year":1994}, {"value":44000,"year":1999}, {"value":25000,"year":2001},{"value":45500,"year":2014}, {"value":75000,"year":2012}];

        var margin = { top: 20, right: 20, bottom: 30, left: 80 },
            width = 500 - margin.left - margin.right,
            height = 250 - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
            .rangeRoundBands([0, width], .1);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom");

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(10);

        var svg = d3.select(this.elementRef.nativeElement).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

x.domain(dataset.map(function(d) { return d.year; }));
  y.domain([0, d3.max(dataset, function(d) { return d.value; })]);

var mapMouseOver = (d: any) => {
    $('.'+String(d.year)+'-'+this.id).addClass('info');
     d3.selectAll($("#" + d.id))
    .style("fill", "red")
    .style("stroke", "blue");
}
var mapMouseOut = (d : any) => {
        $('.'+String(d.year)+'-'+this.id).removeClass('info');

}
  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(this.yTitle);

  svg.selectAll(".bar")
      .data(dataset)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.year); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.value); })
      .attr("height", function(d) { return height - y(d.value); }).on("mouseover", mapMouseOver).on("mouseout", mapMouseOut)
















        //Width and height
        var w = 500;
        var h = 100;
        var barPadding = 1;







        //Create SVG element



        /*  var svg = d3.select(this.elementRef.nativeElement)
              .append("svg")
              .attr("width", w)
              .attr("height", h);
  
  
          svg.selectAll("rect")
              .data(dataset)
              .enter()
              .append("rect")
              .attr("x", function (d, i) {
                  return i * (w / dataset.length);
              })
              .attr("y", function (d) {
                  return h - (d * 4);
              })
              .attr("width", w / dataset.length - barPadding)
              .attr("height", function (d) {
                  return d * 4;
              })
              .attr("fill", function (d) {
                  return "rgb(0, 0, " + (d * 10) + ")";
              });
  
             svg.selectAll("rect")
              .data(dataset)
              .enter()
              .append("rect")
              .attr("x", function (d, i) {
                  return i * (w / dataset.length);
              })
              .attr("y", function (d) {
                  return h - (d * 4) + 1;
              })
              .attr("width", w / dataset.length - barPadding)
              .attr("height", function (d) {
                  return d * 4;
              })
              .attr("fill", function (d) {
                  return "rgb(0, 0, " + (d * 10) + ")";
              });
  
          svg.selectAll("text")
              .data(dataset)
              .enter()
              .append("text")
              .text(function (d) {
                  return '$'+d*10000;
              })
              .attr("text-anchor", "middle")
              .attr("x", function (d, i) {
                  return i * (w / dataset.length) + (w / dataset.length - barPadding) / 2;
              })
              .attr("y", function (d) {
                  return h - (d * 4) + 14;
              })
              .attr("font-family", "sans-serif")
              .attr("font-size", "11px")
              .attr("fill", "white")
         
*/
    }
    ngAfterContentInit() {
    }
}






