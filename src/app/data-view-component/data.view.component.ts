import { Component, Input, OnInit, forwardRef } from '@angular/core';
import {ROUTER_DIRECTIVES, Params, Router, ActivatedRoute} from '@angular/router';



import { parcel, parcelService } from '../parcel-component/parcel.class';
import {Carousel} from '../image-carousel-component/image.carousel';
import {DetailHeader} from '../details-header-component/details.header';
import {DetailTable} from '../details-table-component/details.table';
import {d3graph} from '../graph.component';
import {mapView} from '../mapview-component/map.view.component';
@Component({
    selector: 'modalView',
    styles: [`     .container{
        padding-top:20px;
      }
      .carousel {
          display:none;

      }
    `],
    template: `
    
    <div class="container-fluid">
  
    <div class="row">
             <div class="col-sm-8">
             <carousel class="imgs" lat="{{lat}}" lng="{{lng}}"></carousel>
             <d-header  class="header" address="{{address}}" owner="{{owner}}" zip="{{zip}}"  assesment="{{assesment}}" change="{{change}}"></d-header>
             <d-table class="table" LotSize="{{LotSize}}" parcelId="{{parcelId}}" owner="{{owner}}" assesment="{{assesment}}" yearBuilt="{{yearBuilt}}"
             bedrooms="{{bedrooms}}" bathrooms="{{bathrooms}}" garage="{{garage}}" assesmentYear="{{assesmentYear}}"
             landValue="landValue" improvementValue="{{improvementValue}}" totalValue="{{totalValue}}" elementary="{{elementary}}"
             middleSchool="{{middleSchool}}" highSchool="{{highSchool}}" dateSale="{{dateSale}}" salePrice="{{salePrice}}" 
             book="{{book}}"></d-table>
            </div>
            <div class="col-sm-4">
            
            
        </div>
        
    </div>
  </div>
  
    
    
    `,
    providers: [parcelService],
    directives: [d3graph, Carousel, DetailHeader, DetailTable, ROUTER_DIRECTIVES],
})
export class modalView {
    private parcelData: parcel[] = [];
    @Input() lat: number;
    @Input() lng: number;
    @Input() address: string;
    @Input() owner: string;
    @Input() assesment: string;
    @Input() change: string;
    @Input() zip: string;
    @Input() LotSize: string;
    @Input() parcelId: string;
    @Input() yearBuilt: string;
    @Input() bedrooms: string;
    @Input() bathrooms: string;
    @Input() garage: string;
    @Input() assesmentYear: string;
    @Input() landValue: string;
    @Input() improvementValue: string;
    @Input() totalValue: string;
    @Input() elementary: string;
    @Input() middleSchool: string;
    @Input() highSchool: string;
    @Input() dateSale: string;
    @Input() salePrice: string;
    @Input() book: string;
    @Input() fieldPairs: Object;
    @Input() id: string;
    @Input() finished: boolean = false;


    ngOnInit() {
    }


}
/**
 * dataview -- visualizes parcel views 
 */
@Component({
    selector: 'dataView',
    styles: [`     .container{
        padding-top:20px;
      }


      @media(max-width: 480px){
         .school{
           padding-top:30px;
           
             
         }
         body{
             background:white;
         }
         

      }





    `],
    template: require('./parcelview.html'),
    providers: [parcelService],
    directives: [Carousel, DetailHeader, forwardRef(() => mapView), DetailTable, ROUTER_DIRECTIVES]
})
export class dataView {

    width = window.innerWidth;

    private id: string;
    coords: any;
    lng: any;
    lat: any;
    address = "";
    fields: Object;
    owner: string;
    assesment: string;
    change: string;
    zip: string;
    LotSize: string;
    parcelId: string;
    yearBuilt: string;
    bedrooms: string;
    bathrooms: string;
    garage: string;
    assesmentYear: string;
    landValue: string;
    improvementValue: string;
    totalValue: string;
    elementary: string;
    middleSchool: string;
    highSchool: string;
    dateSale: string;
    salePrice: string;
    book: string;
    fieldPairs: Object;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private service: parcelService, public parcels: parcelService) {
        this.sub = this.route.params.subscribe((params) => {
            let id = String(params['id']);
            console.log(this.parcels);
            if (this.parcels != null) {
                parcels.parcels
                    .subscribe(
                    (parcels:any) => {
                        parcels.forEach((element: parcel) => {
                            if (element.getID() == id) {
                                this.displayParcel(element);
                            }
                        });
                    },
                    (error: any) => console.error('Error: ' + error),
                    () => {
                   
                    });


            }



        })
    }

    /*constructor(private routeParams: RouteParams, private parcels: parcelService) {
        this.id = this.routeParams.get('id');
        if ((this.parcelData != null) && (this.id != null)) {
            parcels.parcels
                .subscribe(
                (parcels: any) => this.parcelData = parcels,
                (error: any) => console.error('Error: ' + error),
                () => {
                    this.address = this.id
                    this.parcelData.forEach((p: parcel) => {

                        if (p.getID() == this.id) {
                            this.displayParcel(p)
                        }

                    });
                }
                );
        }

    }*/
    private parcelData: parcel[] = [];
    private sub: any;

    ngDestory() {
        this.sub.unsubscribe();
    }

    displayParcel(p: parcel) {
        this.address = p.getAddress();
        this.fieldPairs = p.getValues(); //gets all the values from the parcel
        this.owner = this.fieldPairs['owner'];
        this.assesment = this.fieldPairs['assesment'];
        this.change = this.fieldPairs['change'];
        this.zip = this.fieldPairs['zip'];
        this.LotSize = this.fieldPairs['LotSize'];
        this.parcelId = this.fieldPairs['parcelId'];
        this.yearBuilt = this.fieldPairs['yearBuilt'];
        this.bedrooms = this.fieldPairs['bedrooms'];
        this.bathrooms = this.fieldPairs['bathrooms'];
        this.garage = this.fieldPairs['garage'];
        this.assesmentYear = this.fieldPairs['assesmentYear'];
        this.landValue = this.fieldPairs['landValue'];
        this.improvementValue = this.fieldPairs['improvementValue'];
        this.totalValue = this.fieldPairs['totalValue'];
        this.elementary = this.fieldPairs['elementary'];
        this.middleSchool = this.fieldPairs['middleSchool'];
        this.highSchool = this.fieldPairs['highSchool'];
        this.dateSale = this.fieldPairs['dateSale'];
        this.salePrice = this.fieldPairs['salePrice'];
        this.book = this.fieldPairs['book'];







    }

}
