import {Component, Input} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';
import{parcel} from '../parcel-component/parcel.class';

@Component({
    selector:'carousel',
    template:`
                    <div  style="padding-top:20px"class="visible-xs visible-sm">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                        <img height="150" width="250"src="../img/house2.jpg" alt="" class="img-responsive">
                        </div>

                        <div class="item">
                        <img height="150" width ="250" src="../img/house1.jpg" alt="" class="img-responsive">
                        </div>

                        <div class="item">
                        <img height="150" width ="250" src="../img/house3.jpg" alt="" class="img-responsive">
                        </div>

                        <div class="item">
                        <img height="150"  width ="250" src="../img/house4.jpg" alt="" class="img-responsive" >
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
            </div>
                    </div>
                    
                    
                    <div class="hidden-xs hidden-sm">
                    <div class="row">
                    <div class="col-sm-3">
                        
                            <img  height="150" width="200" style="padding-bottom:5px" class="img1" src="../img/house2.jpg" class="hvr-float">
                      
                           <img height="150" width="200" class="img2" src="../img/house1.jpg" class="hvr-sink">
                    </div>
                  
                    <div class="col-sm-3">
                 
                         <img height="150" width="250" style="padding-bottom:5px ; padding-left:20px" class="img3" src="http://maps.googleapis.com/maps/api/streetview?size=400x400&location={{lat}},{{lng}}
                            &fov=90&heading=235&pitch=10
                            &key=AIzaSyAX0zmMwaB_kiismtC6N1YCq3ZlfL5cxQI"class="hvr-float">
                    
                    
                        <img height="150" width="250" style="padding-left:20px" class="img4" src="../img/house4.jpg" class="hvr-sink">
                    </div>
                   
                   <div class="col-sm-6" >
                   
                    <img  class="hvr-grow" style="padding-left:70px" src= "http://maps.googleapis.com/maps/api/staticmap?center={{lat}},{{lng}}&zoom=18&size=300x300&markers=color:pruple|{{lat}},{{lng}}&key=AIzaSyAX0zmMwaB_kiismtC6N1YCq3ZlfL5cxQI">
                 

                   </div>
                    </div>
                    </div>
                    
                    
                    
                    
                    
            `,
            styles:[`
                 img{
                    postion:fixed;
                    

                 
                }
                .img1{
                    
                    height:150px;
                  
                    
                }
                .img2{
                   height:150px;
                     
                }
                  .img3{
                    height:150px;
                   
                     height:150px;
                     
                }
                   .img4{
                     height:150px;
                     
                    
                    
                }
              
            @media(max-width:480px){
                .row{

                    display:none;
                }
                

            }
            
            
            `]
            
})


export class Carousel{
    
@Input() lat: number;
@Input() lng: number;

}

