import {Component, OnInit} from '@angular/core';
import {ROUTER_DIRECTIVES, Params, Router, ActivatedRoute } from '@angular/router';
import {GoogleMapComponent} from '../googlemap-component/googlemap.component';
import {toggleBtn} from '../togglelayers-component/togglelayers.component';
import {parcelService} from '../parcel-component/parcel.class';

import { searchBar } from '../search-component/search.component';
declare var $: any;

import {modal} from '../modal-component/modal.component';
@Component({
    selector: 'mapView',
    styles: [`
      nav { margin-bottom: 0px; }

/* Toggle Styles     border-top-left-radius: 2em;
 */

#wrapper {
    padding-left: 0;
  
    height:95vh;
}
.panel-menu {
    background-color:  rgb(120, 195, 93);
}
#wrapper.toggled {
    padding-left: 250px;
    
}
img: first-child  {
     border-top-left-radius: 2em;
}
#sidebar-wrapper {
    z-index: 1000;
    width: 0;
    height: 100%;
    overflow-y: auto;
   
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}

#wrapper.toggled #sidebar-wrapper {
    width: 250px;
}

#page-content-wrapper {
    width: 100%;
    position: absolute;
    padding: 15px;
}

#wrapper.toggled #page-content-wrapper {

}

img : first-child {
   border-top-left-radius: 2em !important;
}
/* Sidebar Styles */

.sidebar-nav {
    margin: 0;
    padding: 0;
    list-style: none;
}

.sidebar-nav li {
    text-indent: 20px;
    line-height: 40px;
}

.sidebar-nav li a {
    display: block;
    text-decoration: none;
    color: #999999;
}
.selected-parcel {
   -webkit-transform: translateY(-8px);
  transform: translateY(-8px);
  font-size: 30px;
   border-bottom: thin solid #5bc0de;
}
.sidebar-nav li a:hover {
    text-decoration: none;
    color: #fff;
    background: rgba(255,255,255,0.2);
}

.sidebar-nav li a:active,
.sidebar-nav li a:focus {
    text-decoration: none;
}

.sidebar-nav > .sidebar-brand {
    height: 65px;
    font-size: 18px;
    line-height: 60px;
}

.sidebar-nav > .sidebar-brand a {
    color: #999999;
}

.sidebar-nav > .sidebar-brand a:hover {
    color: #fff;
    background: none;
}

@media(min-width:768px) {


    #wrapper.toggled {
        padding-left: 0;
    }

    #sidebar-wrapper {
        width: 250px;
    }

    #wrapper.toggled #sidebar-wrapper {
        width: 0;
    }

    #page-content-wrapper {
        position: relative;
    }

    #wrapper.toggled #page-content-wrapper {
        position: relative;
        margin-right: 0;
    }
}
.image { 
   position: relative; 
   width: 100%; /* for IE 6 */
   border-top-left-radius: 2em;
   padding-right:3px;
}
 h3 span { 
   position: absolute; 
   left: 0;
   bottom: 0; 
  width: 100%;
  padding-right:5px; 
}
img  {
    width:100%;
}
h3 span a {
  color: white;
  
}
h3 span { 
   color: white; 
   letter-spacing: -1px;  
    background: -webkit-linear-gradient( rgba(0,0,0,0), rgba(0,0,0,1)); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient( rgba(0,0,0,0), rgba(0,0,0,1)); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient( rgba(0,0,0,0), rgba(0,0,0,1)); /* For Firefox 3.6 to 15 */
    background: linear-gradient(to bottom, rgba(0,0,0,0), rgba(0,0,0,1)); /* Standard syntax (must be last) */
     padding: 10px 10px 10px 10px; 
   
}
.row {
 
}
.panel-success {
        opacity: 0.65;
}
.col-sm-9 .col-md-6 .col-lg-9 {
  display:inline-block;
  width:1px;
  height:258px;
}
p:hover {
    font-size: 30px;
}
h3 span p p {
      font-size: small;
}
h3 span p p i {
      font-size: small;
      padding-left: 2px;
      padding-right: 4px;
}
h3 span p p i.fa-square {
font-size: xx-small;
}
h3 span p p img {
      width: 12px;
      height: 12px;
}
#wrapper {
        box-shadow: 12px 0 15px -4px rgba(29, 170, 149, 0.8), -12px 0 8px -4px rgba(6, 85, 109, 0.8);
        background:#D7E0E3;
}
p{
    font-weight:500;
}
.scrollarea {
      overflow-y: scroll;
    overflow-x: hidden;
    height:89vh;
}

#ex1Slider .slider-selection {
	background: #BABABA;
}

 

  `],
    template: require('./mapView.html'),
    directives: [GoogleMapComponent, toggleBtn, modal, searchBar, ROUTER_DIRECTIVES],
    providers: [GoogleMapComponent, parcelService]
})

export class mapView {
    public parcelInfo: any[];
    // key = "AIzaSyDHCfV_33C4gDl_OeHVmpdYawORwjwVM6A"
    key = "AIzaSyA-70ObVWMGDjN-1c8rd5F1RmM39oQotZ4";
    mapOptions = {
        center: { lat: 37.533169, lng: -77.431721 },
        scrollwheel: false,
        zoom: 8
    }
    selectedParcel = "";
    googleLoaded = false;

    constructor(private route: ActivatedRoute,
        private router: Router,
        public parcels: parcelService) {
        this.sub = this.route.params.subscribe(params => {
            let id = params['id']; // (+) converts string 'id' to a number
            this.selectedParcel = id;
            console.log(id);

        });
        //did we search for parcel?q2

        let parcelData = new Array() //angular2 is stupid about forEach
        let originalParcels = new Array();
        if (this.parcels != null) {
            parcels.parcels
                .subscribe(
                (parcels: any) => {

                    parcels.forEach((element: any) => {
                        parcelData.push(element.getSearchValues());
                        originalParcels.push(element)
                    });
                },
                (error: any) => console.error('Error: ' + error),
                () => {
                    this.parcelInfo = originalParcels;

                    console.log(this.parcelInfo)
                });


        }
    }
    onGoogleMapsLoad(e: any) {
        this.googleLoaded = true;
        $("#loadingScreen").hide()
        $(".scrollarea").removeClass('hide');
    }
    private sub: any;



    ngOnInit() {
     

       $("button").click(function(){
    $(".well").fadeToggle("slow");

});
$("#ex2").slider({});

$("#datepicker").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
});
$("#datepicker2").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
});





    }

    recenterMap(p: any) {
        console.log('selecter parcel', p)
        $('.hvr-float').css('  -webkit-transform', 'translateY(-8px)');
        $('.hvr-float').css('  transform', 'translateY(-8px) ');
        this.selectedParcel = p;
    }
}