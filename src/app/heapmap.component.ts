import {Component, Input, AfterContentInit, OnInit, ElementRef}  from '@angular/core';
declare var $: any;
import {parcel, parcelService } from './parcel-component/parcel.class'
declare var d3: any;
@Component({
    selector: 'heatmap',
    template: ``,
    styles: [`
height: 100%;
    width: 100%;

    `],
    properties: ['lat', 'lng', 'parcels', 'id']
})
export class heatMapComponent implements AfterContentInit, OnInit {
    map: any;
    heatmap: any;
    id: string;
    elementRef: ElementRef;
    parcelInfo: any;
    yTitle: string = "assesment vale";
    ngOnInit() {
        if (this.hasOwnProperty('parcelId')) {
            this.id = this['parcelId']
        } else {
            this.id = ''
        }


    }
    ngAfterContentInit() {

    }
}