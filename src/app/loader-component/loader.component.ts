import {Http, Response,HTTP_PROVIDERS} from '@angular/http';
import {parcel} from '../parcel-component/parcel.class';
import { Observable }     from 'rxjs/Observable';
import {Inject,Component} from '@angular/core';
import {Injectable} from '@angular/core';
// Statics
import 'rxjs/add/observable/throw';

// Operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map'




@Injectable()
export class parcelLoader {
  data: any;
  constructor(http: Http) {
    console.log("loading");
    http.get('data.json')
      // Call map on the response observable to get the parsed data object
      .map(res => res.json())
      // Subscribe to the observable to get the parsed data object and attach it to the
      // component
      .subscribe(data => this.data = data);
  }
}
/*



/**
 *  Loads JSON parcel data from remote source 
 *  @param{string} url 
 *  methods:
 *  getData() -- performs AJAX request to get parcel data
 */
/*
@Injectable()
export class parcelLoader {
  constructor(http: Http, url: string) {
    this.url = url;
    // this.getData(http);
  }
  data: Object;
  private url: string; //URL to load from
  getData(http:Http): Observable<parcel[]> {
    return http.get(this.url)
      .map(this.extractData)
      .catch(this.handleError);
  }
  private extractData(res: Response) {
    let data = res.json();
    return new parcel(data)
  }
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console
    return Observable.throw(errMsg);
  }
}

*/