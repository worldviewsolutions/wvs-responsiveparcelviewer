import { RouteConfig, Router, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
// Add these symbols to override the `LocationStrategy`
import { provide }           from '@angular/core';
import { LocationStrategy,
        HashLocationStrategy } from '@angular/common';
import { Component } from '@angular/core'
import { mapView } from './map.view.component';
import { dataView } from './data.view.component'
import { searchBar } from './search.component';
import { disclaimerView } from './disclaimerView.component';
@Component({
  selector: 'app',
   styles: [`   
  .navbar  {
    margin-bottom: 20px;

    }
    
    #search {
      width: 500px !important;
    }
   `],
  template: `
<nav *ngIf="navbarVisible"  class="hidden-sm hidden-xs navbar navbar-inverse ">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">{{title}} </a>
      
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a  [routerLink]="['Main']"><i class="fa fa-map" aria-hidden="true"></i> Map</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
          Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="./parcel/">Parcel</a></li>
            <li><a href="#">View Parcel</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div  class="form-group">
          <search-bar></search-bar>
        </div>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">County Website</a></li>
      </ul>
    </div>
  </div>
</nav>
<div class="hidden-lg hidden-md">
  <search-bar></search-bar>
  </div>
  <div class="container-fluid">
    <router-outlet></router-outlet>
    </div>
  `,
  directives: [ROUTER_DIRECTIVES,searchBar],
  providers: [
    ROUTER_PROVIDERS,
    mapView,
    dataView
  ]
})
@RouteConfig([
  {
    path: '/',
    name: 'Main',
    useAsDefault: true,
    component: disclaimerView
  },
    {
    path: '/map/:id',
    name: 'Map',
    component: mapView
  },
    {
    path: '/parcel/:id',
    name: 'Parcel',
    component: dataView,
  }
])
export class AppComponent {
  title = 'City of Richmond';
  navbarVisible = false;
  constructor(private router: Router) {
    router.subscribe((route) => {
      if (route.includes('map') || route.includes('parcel')) {
        this.navbarVisible = true;
      }
    })
  }

}
