import {Component, Injectable, OnInit, Input} from '@angular/core';
import { parcel, parcelService } from '../parcel-component/parcel.class';
import { modalView } from '../data-view-component/data.view.component';

@Component({
  selector: 'modal',
  template: `
    <!-- Modal -->
<div id="{{id}}" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
    <i class="fa fa-print" aria-hidden="true"></i>

        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body">
      <div class="container">
      <div style="overflow-x: hidden;" class="col-md-12 col-lg-12">
      <modalView id="{{id}}" zip="{{zip}}"
  lat="{{lat}}"
  lng="{{lng}}"
  LotSize="{{LotSize}}"
  parcelId="{{parcelId}}"
  yearBuilt="{{yearBuilt}}"
  bedrooms="{{bedrooms}}"
  bathrooms="{{bathrooms}}"
  change="{{change}}"
  garage="{{garage}}"
  assesmentYear="{{assesmentYear}}"
  landValue="{{landValue}}"
  improvementValue="{{improvementValue}}"
  totalValue="{{totalValue}}"
  elementary="{{elementary}}"
  middleSchool="{{middleSchool}}"
  highSchool="{{highSchool}}"
  dateSale="{{dateSale}}"
  salePrice="{{salePrice}}"
  book="{{book}}"
  id="{{id}}"
  address = "{{address}}"
  owner="{{owner}}"
  assesment="{{assesment}}"></modalView>
      </div>
      </div>
      </div>
      <div class="modal-footer">
              <img align="left" src="http://gis.venangopa.us/ParcelViewer/Images/WVSLogo.gif"/>  <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
`,
  providers: [parcelService],
  directives: [modalView],
  styles: [`

    `],
  properties: ['parcel', 'address']
})

@Injectable()
export class modal implements OnInit {
  fieldPairs: Object;
  change: string;
  zip: string;
  LotSize: string;
  parcelId: string;
  yearBuilt: string;
  bedrooms: string;
  bathrooms: string;
  garage: string;
  assesmentYear: string;
  landValue: string;
  improvementValue: string;
  totalValue: string;
  elementary: string;
  middleSchool: string;
  highSchool: string;
  dateSale: string;
  salePrice: string;
  book: string;
  id: string;
  parcel: parcel;
  address: string;
  fields: Object;
  owner: string;
  assesment: string;
  coords: any;
  private lng:number;
  private lat:number;
  polygon:any;
  parcelData = new Array();
  
  constructor() {
  }
 
 
 
 
  ngOnInit() {
    this.address = this['address'];
    this.id = this['parcel'].getID();
    this.displayParcel(this['parcel'])
    } 
  displayParcel(p: parcel) {
    this.address = p.getAddress();
    this.fieldPairs = p.getValues(); //gets all the values from the parcel
    this.owner = this.fieldPairs['owner'];
    this.assesment = this.fieldPairs['assesment'];
    this.change = this.fieldPairs['change'];
    this.zip = this.fieldPairs['zip'];
    this.LotSize = this.fieldPairs['LotSize'];
    this.parcelId = this.fieldPairs['parcelId'];
    this.yearBuilt = this.fieldPairs['yearBuilt'];
    this.bedrooms = this.fieldPairs['bedrooms'];
    this.bathrooms = this.fieldPairs['bathrooms'];
    this.garage = this.fieldPairs['garage'];
    this.assesmentYear = this.fieldPairs['assesmentYear'];
    this.landValue = this.fieldPairs['landValue'];
    this.improvementValue = this.fieldPairs['improvementValue'];
    this.totalValue = this.fieldPairs['totalValue'];
    this.elementary = this.fieldPairs['elementary'];
    this.middleSchool = this.fieldPairs['middleSchool'];
    this.highSchool = this.fieldPairs['highSchool'];
    this.dateSale = this.fieldPairs['dateSale'];
    this.salePrice = this.fieldPairs['salePrice'];
    this.book = this.fieldPairs['book'];
            this.coords=p.getcoords();
        console.log('coords',this.coords);
        this.lat=this.coords.lat();
        this.lng=this.coords.lng();



  }
}