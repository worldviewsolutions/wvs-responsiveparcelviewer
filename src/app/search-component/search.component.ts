import {Component, EventEmitter, OnInit} from '@angular/core';
import { parcel, parcelService } from '../parcel-component/parcel.class';
import {AutocompleteDirective} from "../ng2-autocomplete/autocomplete";
import {Http, HTTP_PROVIDERS} from "@angular/http";

import {ROUTER_DIRECTIVES, Router} from '@angular/router';
import "rxjs/Rx";
declare var $: any;
declare var window: any;
@Component({
    selector: 'search-bar',
    //        <!--    padding: 12px 20px; -->                     padding: 22px 20px !important;

    styles: [`
      #search {
            z-index:99999;

            box-sizing: border-box;
           }
           .dropdown-item {
               font-size: 30px;
           }
           .search-results {
               font-size:30px !important;
               position:absolute;
           }
          input[type=text] {
              }
           input[type=text]:focus {
                border: 3px solid lightgrey;
            }
          
  `],
    directives: [AutocompleteDirective, ROUTER_DIRECTIVES],
    providers: [parcelService],
    outputs: ['grayedOut'],
    template: `
  <div class="row"><div class="col-md-10"><input id='search' class="form-control" type="text" [ng2-autocomplete]="searchParcel()" (ng2AutocompleteOnSelect)="onParcelSelected($event)" [(ngModel)]="countryName" placeholder="search for addresses or parcel ids here" type="text" autocomplete="off"></div><div class="col-md-2">      <button class="btn btn-primary"><i class="fa fa-location-arrow" aria-hidden="true"></i>
</button> </div></div>`
})
export class searchBar implements OnInit {



    public countryName = "";
    public query: string = '';
    public filteredList: string[];
    private parcelAddresses: string[];
    public elementRef: any;
    constructor(private http: Http, private router: Router, private service: parcelService) {

    }
    public searchParcel() {
        return (filter: string): Promise<Array<{ text: string, data: any }>> => {
            return new Promise<Array<{ text: string, data: any }>>((resolve, reject) => {
                let parcelURL: string;
                //fix for live and testing
                if (window.location.hostname === 'localhost') {
                    parcelURL = "http://localhost:8080/parcels.json";
                } else {
                    parcelURL = "http://vm102.worldviewsolutions.net/mobilePV/parcels.json";
                }
                this.http.get(parcelURL) //+filter
                    .map((res: any) => res.json().parcels)
                    .map(parcels => parcels.map((parcel: any) => {
                        console.log(parcel);
                        return { text: parcel.fields.address, data: parcel.parcelId };
                    }))
                    .subscribe(
                    parcels => resolve(parcels),
                    err => reject(err),
                    () => { }
                    );
            });
        };
    }


    ngOnInit() {
        $("input").click(function () {
            $(".image").css("opacity", ".4");
        });
        $("input").keydown(function () {
            $(".image").css("opacity", ".4");
            $(".scrollarea").hide();
        });
        $("body").keyup(function () {
            $(".image").css("opacity", "10");
          
        });
        $("body").mouseup(function () {
            $(".image").css("opacity", "10");
              $(".scrollarea").show();
        });

    }


    public onParcelSelected(selected: { text: string, data: any }) {
        let link = ['/map', selected.data];
        this.router.navigate(link);

        //this.router.navigate(['map', { id: selected.data }]);
        // window.location = './map/'+ selected.data;
    }

}









