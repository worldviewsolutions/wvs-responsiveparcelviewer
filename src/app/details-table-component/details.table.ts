import {Component, Input} from '@angular/core';
import {d3graph} from '../graph.component';
import {heatMapComponent} from '../heapmap.component';
@Component({
    selector: 'd-table',
    directives: [d3graph, heatMapComponent],
    template: require('./table.html'),
    styles: [`
           
           .school{
               
           }
           


           
           @media(max-width: 480px){
         .row{
           padding-top:30px;
           
             
         }
         @media(max-width: 790px){
         .row{
           padding-top:30px;
           
             
         }
    `]


})


export class DetailTable {
    @Input() assesment: string;
    @Input() LotSize: string;
    @Input() parcelId: string;
    @Input() yearBuilt: string;
    @Input() bedrooms: string;
    @Input() bathrooms: string;
    @Input() garage: string;
    @Input() assesmentYear: string;
    @Input() landValue: string;
    @Input() improvementValue: string;
    @Input() totalValue: string;
    @Input() elementary: string;
    @Input() middleSchool: string;
    @Input() highSchool: string;
    @Input() dateSale: string;
    @Input() salePrice: string;
    @Input() owner: string;
    @Input() book: string;



}





