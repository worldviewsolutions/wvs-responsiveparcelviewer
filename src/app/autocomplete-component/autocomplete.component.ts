
import {Component, Host, OnInit} from '@angular/core';
import { parcel, parcelService } from '../parcel-component/parcel.class';
declare var $: any;
@Component({
    selector: 'autocomplete',
    template: `
        <div class="container" >
            <div class="input-field col s12">
              <input id="item" type="text" class="validate filter-input" [(ngModel)]=query (keyup)=filter()>
              <div class="btn-group" role="group" aria-label="...">
              
            </div>
            <div class="suggestions" *ngIf="filteredList.length > 0">
                <ul *ngFor="let item of filteredList" >
                    <li >
                        <a (click)="select(parcel)">{{parcel.id}}</a>
                    </li>
                </ul>
            </div>
        </div>  	
        `,
    host: {
        '(document:click)': 'handleClick($event)',
    },
providers: [
  parcelService
  ]
})








export class autocomplete implements OnInit {
    private query: string;
    private filteredList: any[];
    private parcelAddresses: string[];




    constructor(private parcels: parcelService) {
        parcels.parcels
            .subscribe(
            (parcels: any) => {
                parcels.forEach((parcel: parcel) => {
                    this.parcelAddresses.push(parcel.getAddress())
                })
            },
            (error: any) => console.error('Error: ' + error),
            () => {
                console.log(this.parcelAddresses)
            }
            );
    }

    ngOnInit() {
        console.log($('input[class*="filter"]').width());
        $(".search-results").width($('input[class*="filter"]').width());
    }

    filter() {
        if (this.query !== "") {
            this.filteredList = this.parcelAddresses.filter(function (el:any) {
                return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
            }.bind(this));
        } else {
            this.filteredList = [];
        }

    }

    select(item:any) {
        this.query = item;
        this.filteredList = [];
    }

    handleClick(event:any) {

    }
}